﻿namespace SportSimulator
{
    using Microsoft.Extensions.DependencyInjection;
    using SportSimulator.Data;
    using SportSimulator.Generators;
    using SportSimulator.Loggers;
    using SportSimulator.Models;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public class Program
    {
        private static Random Random => new Random();
        private static IConferenceRepository _conferenceRepository;
        private static IDivisionRepository _divisionRepository;
        private static ILeagueRepository _leagueRepository;
        private static IGameRepository _gameRepository;
        private static ITeamRepository _teamRepository;
        private static ISeasonRepository _seasonRepository;
        private static IPostSeasonGameGenerator _postSeasonGameGenerator;
        private static IFileLogger _fileLogger;

        public static async Task Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton<IConferenceRepository, NflConferenceRepository>()
                .AddSingleton<IDivisionRepository, NflDivisionRepository>()
                .AddSingleton<IGameRepository, NflGameRepository>()
                .AddSingleton<ILeagueRepository, NflLeagueRepository>()
                .AddSingleton<ITeamRepository, NflTeamRepository>()
                .AddSingleton<ISeasonRepository, NflSeasonRepository>()
                .AddSingleton<IPostSeasonGameGenerator, PostSeasonGameGenerator>()
                .AddSingleton<IFileLogger, FileLogger>()
                .BuildServiceProvider();

            _conferenceRepository = serviceProvider.GetService<IConferenceRepository>();
            _divisionRepository = serviceProvider.GetService<IDivisionRepository>();
            _leagueRepository = serviceProvider.GetService<ILeagueRepository>();
            _gameRepository = serviceProvider.GetService<IGameRepository>();
            _teamRepository = serviceProvider.GetService<ITeamRepository>();
            _seasonRepository = serviceProvider.GetService<ISeasonRepository>();
            _postSeasonGameGenerator = serviceProvider.GetService<IPostSeasonGameGenerator>();
            _fileLogger = serviceProvider.GetService<IFileLogger>();

            if (File.Exists(Constants.OutputFile))
            {
                File.Delete(Constants.OutputFile);
            }
            var year = 2019;

            var nflTeams = await _teamRepository.GetTeamsAsync();
            var nfl = (await _leagueRepository.GetLeaguesAsync()).First();
            var nflDivisions = await _divisionRepository.GetDivisionsAsync();
            var nflConferences = await _conferenceRepository.GetConferencesAsync();

            var haveSeasons = true;
            var seasonsToPlay = Constants.SeasonsToPlay;
            do
            {
                Console.Write("How many seasons do you want to play: ");
                haveSeasons = int.TryParse(Console.ReadLine(), out seasonsToPlay);

            } while (!haveSeasons);
            
            for (var i = 0; i < seasonsToPlay; i++)
            {
                var date = new DateTimeOffset(new DateTime(year, 9, 5));
                var seasonInfo = new SeasonInfo
                {
                    Name = $"{year++} Season",
                    LeagueId = nfl.Id,
                    StartDate = date
                };
                var seasonId = await _seasonRepository.CreateSeasonAsync(seasonInfo);
                await _fileLogger.WriteLineAsync($"{seasonInfo.Name}\n");
                //await WriteLineAsync($"{seasonInfo.Name}\n");
                await GenerateNflSeasonAsync(nflDivisions, nflTeams, nflConferences, seasonId, date);
                await _fileLogger.WriteLineAsync(Constants.Divider);
            }

            Console.WriteLine("Done.");
            Console.Read();
        }

        private static async Task GenerateNflSeasonAsync(List<Division> nflDivisions, List<Team> nflTeams, List<Conference> nflConferences, int seasonId, DateTimeOffset gameDate)
        {
            await GenerateNflGamesAsync(nflConferences, nflDivisions, nflTeams, seasonId, gameDate);

            var nflGames = await _gameRepository.FindGamesAsync(seasonId, GameType.RegularSeason, null);

            var displayOutcome = false;

            if (displayOutcome)
            {
                await _fileLogger.WriteLineAsync($"\n\nTotalGames: {nflGames.Count()}");
                await _fileLogger.WriteLineAsync($"TotalWeeks: {nflGames.Count() / 16}\n");
                await _fileLogger.WriteLineAsync($"{Constants.Divider}\n{Constants.Divider}\n");
            }

            await DisplayWeekOutcomeAsync(nflGames, gameDate);

            gameDate = gameDate.AddDays(7 * 16);

            await DisplayDivisionOutcomeAsync(nflDivisions, nflGames, nflTeams);
            //await GeneratePlayoffGamesAsync(nflConferences, nflDivisions, nflTeams, nflGames, seasonId, gameDate);
            await _postSeasonGameGenerator.GeneratePostSeaonGamesAsync(seasonId, gameDate);
            await DisplayPlayoffOutcomeAsync(seasonId, gameDate, nflTeams);
        }

        private static async Task DisplayWeekOutcomeAsync(List<Game> games, DateTimeOffset seasonStart)
        {
            for (var i = 0; i < 16; i++)
            {
                var gamesInWeek = games.Where(g => g.StartTime == seasonStart).ToList();
                Console.WriteLine($"Week {i + 1} ({seasonStart}) games: {gamesInWeek.Count}");

                seasonStart = seasonStart.AddDays(7);
            }

            await Task.CompletedTask;
        }

        private static async Task DisplayPlayoffOutcomeAsync(int seasonId, DateTimeOffset gameDate, List<Team> teams)
        {
            var postSeasonGames = await _gameRepository.FindGamesAsync(seasonId, GameType.PostSeason, null);

            var wildCardGames = postSeasonGames.Where(g => g.StartTime == gameDate).ToList();
            await DisplayPlayoffWeekOutcomeAsync("Wild Card Round", wildCardGames, teams);
            gameDate = gameDate.AddDays(7);
            var divisionalGames = postSeasonGames.Where(g => g.StartTime == gameDate).ToList();
            await DisplayPlayoffWeekOutcomeAsync("Divisional Round", divisionalGames, teams);
            gameDate = gameDate.AddDays(7);
            var conferenceChampionshipGames = postSeasonGames.Where(g => g.StartTime == gameDate).ToList();
            await DisplayPlayoffWeekOutcomeAsync("Conference Championship", conferenceChampionshipGames, teams);
            gameDate = gameDate.AddDays(7);
            var superBowl = postSeasonGames.Where(g => g.StartTime == gameDate).ToList();
            await DisplayPlayoffWeekOutcomeAsync("SuperBowl", superBowl, teams);
        }

        private static async Task DisplayPlayoffWeekOutcomeAsync(string title, List<Game> games, List<Team> teams)
        {
            await _fileLogger.WriteLineAsync(title);

            foreach (var game in games)
            {
                var homeTeam = teams.First(t => t.Id == game.HomeTeamId);
                var visitingTeam = teams.First(t => t.Id == game.VisitorTeamId);
                var victoriousTeam = teams.First(t => t.Id == game.VictoriousTeamId);
                await _fileLogger.WriteLineAsync($"{homeTeam.City} {homeTeam.Name} vs {visitingTeam.City} {visitingTeam.Name} -- {victoriousTeam.City} {victoriousTeam.Name}");
            }
            await _fileLogger.WriteLineAsync(Constants.Divider);
        }

        private static async Task DisplayDivisionOutcomeAsync(List<Division> nflDivisions, List<Game> nflGames, List<Team> nflTeams)
        {
            foreach (var division in nflDivisions)
            {
                var divisionTeams = nflTeams.Where(t => t.DivisionId == division.Id);
                var winRecords = new List<int>();
                await _fileLogger.WriteLineAsync($"{division.Name} Records \n");
                foreach (var team in divisionTeams)
                {
                    var wins = nflGames.Where(g => g.VictoriousTeamId == team.Id).Count();
                    team.DivisionRank = wins;
                    winRecords.Add(wins);
                }

                divisionTeams = divisionTeams.OrderByDescending(t => t.DivisionRank).ToList();
                var teamIndex = 0;
                foreach (var team in divisionTeams)
                {
                    team.DivisionRank = ++teamIndex;
                    var wins = nflGames.Where(g => g.VictoriousTeamId == team.Id).Count();
                    
                    var totalGames = nflGames.Where(g => g.HomeTeamId == team.Id || g.VisitorTeamId == team.Id).Count();

                    var homeGames = nflGames.Where(g => g.HomeTeamId == team.Id);
                    var visitingGames = nflGames.Where(g => g.VisitorTeamId == team.Id);

                    var homeWins = homeGames.Where(g => g.VictoriousTeamId == team.Id).Count();
                    var visitingWins = visitingGames.Where(g => g.VictoriousTeamId == team.Id).Count();

                    await _fileLogger.WriteLineAsync($"{team.City} {team.Name}: {wins}-{totalGames - wins} Home: {homeWins}-{homeGames.Count() - homeWins} Visiting: {visitingWins}-{visitingGames.Count() - visitingWins}");
                }

                await _fileLogger.WriteLineAsync($"{Constants.Divider}\n");
            }
        }

        private static async Task<int> GenerateVictoriousTeamAsync(Team homeTeam, Team visitorTeam, bool displayOutcome, int homeWinChance)
        {
            var victoriousTeam = Random.Next(0, 100) < homeWinChance ? homeTeam : visitorTeam;

            if (displayOutcome)
            {
                await _fileLogger.WriteLineAsync($"Home: {homeTeam.City} {homeTeam.Name}; Visitor: {visitorTeam.City} {visitorTeam.Name}; Winner: {victoriousTeam.City} {victoriousTeam.Name};");
            }

            return victoriousTeam.Id;
        }

        private static async Task GeneratePlayoffGamesAsync(List<Conference> conferences, List<Division> divisions, List<Team> teams, List<Game> regularSeasonGames, int seasonId, DateTimeOffset gameDate)
        {
            var playoffGames = new List<GameInfo>();
            var sideOne = new List<Team>();
            var sideTwo = new List<Team>();
            foreach (var conference in conferences)
            {
                var conferenceDivisions = divisions.Where(d => d.ConferenceId == conference.Id);
                var playoffTeams = new List<Team>();
                foreach (var division in conferenceDivisions)
                {
                    var divisionTeams = teams.Where(t => t.DivisionId == division.Id);
                    var highestWinningTeam = default(Team);
                    var highestWinningCount = -1;
                    foreach (var team in divisionTeams)
                    {
                        var victories = regularSeasonGames.Where(g => g.VictoriousTeamId == team.Id).Count();
                        if (highestWinningCount < victories)
                        {
                            highestWinningTeam = team;
                            highestWinningCount = victories;
                        }
                        else if (highestWinningCount == victories)
                        {
                            if (Random.Next(0, 2) == 0)
                            {
                                highestWinningTeam = team;
                            }
                        }
                    }
                    playoffTeams.Add(highestWinningTeam);
                }

                if (sideOne.Count == 0)
                {
                    sideOne.AddRange(playoffTeams);
                }
                else
                {
                    sideTwo.AddRange(playoffTeams);
                }
            }

            while (sideOne.Count > 1)
            {
                var sideOneTeams = sideOne.ToList();
                var sideTwoTeams = sideTwo.ToList();
                var sideOnePlayOffGames = await GenerateRandomPlayoffWeekAsync(sideOneTeams, Constants.PlayoffSeasonHomeWinChance, gameDate, true);
                var sideTwoPlayOffGames = await GenerateRandomPlayoffWeekAsync(sideTwoTeams, Constants.PlayoffSeasonHomeWinChance, gameDate, true);
                playoffGames.AddRange(sideOnePlayOffGames);
                playoffGames.AddRange(sideTwoPlayOffGames);
                var sideOneVictoriousTeamIds = sideOnePlayOffGames.Select(g => g.VictoriousTeamId.Value).ToList();
                var sideTwoVictoriousTeamIds = sideTwoPlayOffGames.Select(g => g.VictoriousTeamId.Value).ToList();
                sideOneTeams = sideOne.Where(s => sideOneVictoriousTeamIds.Contains(s.Id)).ToList();
                sideTwoTeams = sideTwo.Where(s => sideTwoVictoriousTeamIds.Contains(s.Id)).ToList();
                sideOne = sideOneTeams;
                sideTwo = sideTwoTeams;
                gameDate = gameDate.AddDays(7);
            }
            
            var homeTeam = sideOne.First();
            var awayTeam = sideTwo.First();
            var winningTeam = Random.Next(0, 2) == 0 ? homeTeam : awayTeam;
            playoffGames.Add(
                new GameInfo
                {
                    StartTime = gameDate,
                    GameType = GameType.PostSeason,
                    HomeTeamId = homeTeam.Id,
                    VisitorTeamId = awayTeam.Id,
                    VictoriousTeamId = winningTeam.Id
                }
            );
            await _fileLogger.WriteLineAsync($"SuperBowl bewteewn {homeTeam.City} {homeTeam.Name} and {awayTeam.City} {awayTeam.Name}; Winner: {winningTeam.City} {winningTeam.Name};");
            await _fileLogger.WriteLineAsync($"{winningTeam.City} {winningTeam.Name}", Constants.SuperBowlFile);

            foreach (var game in playoffGames)
            {
                await _gameRepository.CreateGameAsync(game);
            }
        }

        private static async Task<List<GameInfo>> GenerateRandomPlayoffWeekAsync(List<Team> teams, int homeWinChance, DateTimeOffset gameDate, bool displayOutcome = false)
        {
            var randomGames = new List<GameInfo>();
            while (teams.Count() > 0)
            {
                var index = Random.Next(0, teams.Count());
                var homeTeam = teams.ElementAt(index);
                teams.RemoveAt(index);
                index = Random.Next(0, teams.Count());
                var visitorTeam = teams.ElementAt(index);
                teams.RemoveAt(index);
                randomGames.Add(
                    new GameInfo
                    {
                        GameType = GameType.PostSeason,
                        HomeTeamId = homeTeam.Id,
                        VisitorTeamId = visitorTeam.Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(homeTeam, visitorTeam, displayOutcome, homeWinChance)
                    });
            }

            return randomGames;
        }

        private static async Task GenerateNflGamesAsync(List<Conference> conferences, List<Division> divisions, List<Team> teams, int seasonId, DateTimeOffset gameDate)
        {
            var divisionMap = new Dictionary<int, int>
            {
                { 0, 1 },
                { 2, 3 }
            };

            var maps = new Dictionary<int, List<int>>
            {
                { 0, new List<int> { 2, 3 } },
                { 1, new List<int> { 2, 3 } }
            };

            // Generates 6 weeks
            await Generate2019NflDivisionGamesAsync(divisions, teams, seasonId, gameDate);

            gameDate = gameDate.AddDays(7 * 6);
            
            // Generates 4 weeks
            await GenerateNflConferenceGamesAsync(divisionMap, conferences, divisions, teams, seasonId, gameDate);
            
            gameDate = gameDate.AddDays(7 * 4);
            // Generates 6 weeks
            await GenerateNflDivisionGamesAsync(divisionMap, conferences, divisions, teams, seasonId, gameDate);
            
            gameDate = gameDate.AddDays(7 * 4);
            // Generates 2 weeks
            await GenerateNflStandingsGamesAsync(maps, conferences, divisions, teams, seasonId, gameDate);
        }

        private static async Task GenerateNflConferenceGamesAsync(
            Dictionary<int, int> divisionMap,
            List<Conference> conferences,
            List<Division> divisions,
            List<Team> teams,
            int seasonId,
            DateTimeOffset gameDate)
        {
            foreach (var conference in conferences)
            {
                var conferenceDivisions = divisions.Where(d => d.ConferenceId == conference.Id);

                var startHomeGame = false;
                foreach (var map in divisionMap)
                {
                    startHomeGame = !startHomeGame;
                    await GenerateGamesBetweenDivisionsAsync(
                        conferenceDivisions.ElementAt(map.Key),
                        conferenceDivisions.ElementAt(map.Value),
                        teams,
                        seasonId,
                        gameDate,
                        startHomeGame);
                }
            }
        }

        private static async Task GenerateNflDivisionGamesAsync(Dictionary<int, int> divisionMap, List<Conference> conferences, List<Division> divisions, List<Team> teams, int seasonId, DateTimeOffset gameDate)
        {
            var conferenceOneDivisions = divisions.Where(d => d.ConferenceId == conferences.ElementAt(0).Id);
            var conferenceTwoDivisions = divisions.Where(d => d.ConferenceId == conferences.ElementAt(1).Id);

            foreach (var map in divisionMap)
            {
                await GenerateGamesBetweenDivisionsAsync(conferenceOneDivisions.ElementAt(map.Key), conferenceTwoDivisions.ElementAt(map.Value), teams, seasonId, gameDate, true);
                await GenerateGamesBetweenDivisionsAsync(conferenceTwoDivisions.ElementAt(map.Key), conferenceOneDivisions.ElementAt(map.Value), teams, seasonId, gameDate, true);
            }
        }

        private static async Task GenerateGamesBetweenDivisionsAsync(
            Division divisionOne,
            Division divisionTwo,
            List<Team> teams,
            int seasonId,
            DateTimeOffset gameTime,
            bool isDivisionOneHome)
        {
            var divisionOneTeams = teams.Where(t => t.DivisionId == divisionOne.Id).OrderBy(t => t.DivisionRank).ToList();
            var divisionTwoTeams = teams.Where(t => t.DivisionId == divisionTwo.Id).OrderBy(t => t.DivisionRank).ToList();

            for (var divisionOneIndex = 0; divisionOneIndex < divisionOneTeams.Count; divisionOneIndex++)
            {
                isDivisionOneHome = !isDivisionOneHome;
                for (var divisionTwoIndex = 0; divisionTwoIndex < divisionTwoTeams.Count; divisionTwoIndex++)
                {
                    var homeTeam = isDivisionOneHome ? divisionOneTeams.ElementAt(divisionOneIndex) : divisionTwoTeams.ElementAt(divisionTwoIndex);
                    var visitorTeam = !isDivisionOneHome ? divisionOneTeams.ElementAt(divisionOneIndex) : divisionTwoTeams.ElementAt(divisionTwoIndex);

                    var gameInfo = new GameInfo
                    {
                        StartTime = gameTime,
                        GameType = GameType.RegularSeason,
                        SeasonId = seasonId,
                        HomeTeamId = homeTeam.Id,
                        VisitorTeamId = visitorTeam.Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(homeTeam, visitorTeam, false, Constants.RegularSeasonHomeWinChance)
                    };

                    await _gameRepository.CreateGameAsync(gameInfo);

                    isDivisionOneHome = !isDivisionOneHome;
                }

                gameTime = gameTime.AddDays(7);
            }
        }

        private static async Task GenerateSameStandingGamesAsync(
            Division division,
            List<Division> opponentDivisions,
            List<Team> teams,
            int seasonId,
            DateTimeOffset gameDate,
            bool isSelectedHome)
        {
            var divisionTeams = teams
                .Where(t => t.DivisionId == division.Id)
                .OrderBy(t => t.DivisionRank)
                .ToList();
            
            foreach (var d in opponentDivisions)
            {
                if (d.Id != opponentDivisions.First().Id)
                {
                    isSelectedHome = !isSelectedHome;
                }
                var opponentTeams = teams
                    .Where(t => t.DivisionId == d.Id)
                    .OrderBy(t => t.DivisionRank)
                    .ToList();
                for (var index = 0; index < divisionTeams.Count; index++)
                {
                    var homeTeam = isSelectedHome ? divisionTeams.ElementAt(index) : opponentTeams.ElementAt(index);
                    var visitorTeam = !isSelectedHome ? divisionTeams.ElementAt(index) : opponentTeams.ElementAt(index);
                    var gameInfo =
                        new GameInfo
                        {
                            GameType = GameType.RegularSeason,
                            StartTime = gameDate,
                            SeasonId = seasonId,
                            HomeTeamId = homeTeam.Id,
                            VisitorTeamId = visitorTeam.Id,
                            VictoriousTeamId = await GenerateVictoriousTeamAsync(
                                homeTeam, 
                                visitorTeam, 
                                false, 
                                Constants.RegularSeasonHomeWinChance)
                        };
                        
                    await _gameRepository.CreateGameAsync(gameInfo);

                    isSelectedHome = !isSelectedHome;
                }
                gameDate = gameDate.AddDays(7);
            }
        }

        private static async Task GenerateNflStandingsGamesAsync(
            Dictionary<int, List<int>> divisionMap,
            List<Conference> conferences,
            List<Division> divisions,
            List<Team> teams,
            int seasonId,
            DateTimeOffset gameDate)
        {
            foreach (var conference in conferences)
            {
                var isSelectedHome = false;
                var conferenceDivisions = divisions.Where(d => d.ConferenceId == conference.Id).ToList();
                foreach (var map in divisionMap)
                {
                    isSelectedHome =! isSelectedHome;
                    var division = conferenceDivisions.ElementAt(map.Key);
                    var opponentDivisions = new List<Division>();;
                    foreach (var index in map.Value)
                    {
                        opponentDivisions.Add(conferenceDivisions.ElementAt(index));
                    }

                    await GenerateSameStandingGamesAsync(division, opponentDivisions, teams, seasonId, gameDate, isSelectedHome);
                }
            }
        }

        private static async Task Generate2019NflDivisionGamesAsync(
            List<Division> divisions,
            List<Team> teams,
            int seasonId,
            DateTimeOffset gameDate,
            bool displayOutcome = false)
        {
            var weekTwoDate = gameDate.AddDays(7);
            var weekThreeDate = gameDate.AddDays(14);
            var weekFourDate = gameDate.AddDays(21);
            foreach (var division in divisions)
            {
                var divisionTeams = teams.Where(t => t.DivisionId == division.Id);
                var games = new List<GameInfo>
                {
                    new GameInfo
                    {
                        StartTime = gameDate,
                        GameType = GameType.RegularSeason,
                        HomeTeamId = divisionTeams.ElementAt(0).Id,
                        VisitorTeamId = divisionTeams.ElementAt(3).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(0), divisionTeams.ElementAt(3), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate,
                        GameType = GameType.RegularSeason,
                        HomeTeamId = divisionTeams.ElementAt(1).Id,
                        VisitorTeamId = divisionTeams.ElementAt(2).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(1), divisionTeams.ElementAt(2), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(7),
                        GameType = GameType.RegularSeason,
                        HomeTeamId = divisionTeams.ElementAt(0).Id,
                        VisitorTeamId = divisionTeams.ElementAt(2).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(0), divisionTeams.ElementAt(2), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(7),
                        GameType = GameType.RegularSeason,
                        HomeTeamId = divisionTeams.ElementAt(1).Id,
                        VisitorTeamId = divisionTeams.ElementAt(3).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(1), divisionTeams.ElementAt(3), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(14),
                        GameType = GameType.RegularSeason,
                        HomeTeamId = divisionTeams.ElementAt(0).Id,
                        VisitorTeamId = divisionTeams.ElementAt(1).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(0), divisionTeams.ElementAt(1), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(14),
                        GameType = GameType.RegularSeason,
                        HomeTeamId = divisionTeams.ElementAt(2).Id,
                        VisitorTeamId = divisionTeams.ElementAt(3).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(2), divisionTeams.ElementAt(3), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    // -- second part
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(21),
                        GameType = GameType.RegularSeason,
                        VisitorTeamId = divisionTeams.ElementAt(0).Id,
                        HomeTeamId = divisionTeams.ElementAt(3).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(3), divisionTeams.ElementAt(0), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(21),
                        GameType = GameType.RegularSeason,
                        VisitorTeamId = divisionTeams.ElementAt(1).Id,
                        HomeTeamId = divisionTeams.ElementAt(2).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(2), divisionTeams.ElementAt(1), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(28),
                        GameType = GameType.RegularSeason,
                        VisitorTeamId = divisionTeams.ElementAt(0).Id,
                        HomeTeamId = divisionTeams.ElementAt(2).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(2), divisionTeams.ElementAt(0), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(28),
                        GameType = GameType.RegularSeason,
                        VisitorTeamId = divisionTeams.ElementAt(1).Id,
                        HomeTeamId = divisionTeams.ElementAt(3).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(3), divisionTeams.ElementAt(1), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(35),
                        GameType = GameType.RegularSeason,
                        VisitorTeamId = divisionTeams.ElementAt(0).Id,
                        HomeTeamId = divisionTeams.ElementAt(1).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(1), divisionTeams.ElementAt(0), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                    new GameInfo
                    {
                        StartTime = gameDate.AddDays(35),
                        GameType = GameType.RegularSeason,
                        VisitorTeamId = divisionTeams.ElementAt(2).Id,
                        HomeTeamId = divisionTeams.ElementAt(3).Id,
                        VictoriousTeamId = await GenerateVictoriousTeamAsync(divisionTeams.ElementAt(3), divisionTeams.ElementAt(2), displayOutcome, Constants.RegularSeasonHomeWinChance)
                    },
                };

                foreach(var game in games)
                {
                    game.SeasonId = seasonId;
                    await _gameRepository.CreateGameAsync(game);
                }
            }
        }
    }
}
