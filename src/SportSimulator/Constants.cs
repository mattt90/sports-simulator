namespace SportSimulator
{
    public static class Constants
    {
        public const string OutputFile = "outcome.txt";
        public const string SuperBowlFile = "superbowl.txt";
        public const int RegularSeasonHomeWinChance = 60;
        public const int PlayoffSeasonHomeWinChance = 50;
        public const string Divider = "---------------------------------------------------------------------";
        public const int SeasonsToPlay = 1;
    }
}