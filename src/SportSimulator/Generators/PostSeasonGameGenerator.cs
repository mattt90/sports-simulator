namespace SportSimulator.Generators
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SportSimulator.Models;
    using SportSimulator.Data;
    using System.Linq;
    using System.IO;
    using SportSimulator.Loggers;

    public class PostSeasonGameGenerator : IPostSeasonGameGenerator
    {
        public PostSeasonGameGenerator(
            IConferenceRepository conferenceRepository,
            IDivisionRepository divisionRepository,
            ITeamRepository teamRepository,
            IGameRepository gameRepository,
            IFileLogger fileLogger)
        {
            ConferenceRepository = conferenceRepository ?? 
                throw new ArgumentNullException(nameof(conferenceRepository));
            DivisionRepository = divisionRepository ??
                throw new ArgumentNullException(nameof(divisionRepository));
            TeamRepository = teamRepository ??
                throw new ArgumentNullException(nameof(teamRepository));
            GameRepository = gameRepository ??
                throw new ArgumentNullException(nameof(gameRepository));
            FileLogger = fileLogger ??
                throw new ArgumentNullException(nameof(fileLogger));
        }

        public async Task GeneratePostSeaonGamesAsync(int seasonId, DateTimeOffset gameDate, bool displayOutcome = false)
        {
            var conferences = await ConferenceRepository.GetConferencesAsync();
            var divisions = await DivisionRepository.GetDivisionsAsync();
            var teams = await TeamRepository.GetTeamsAsync();
            var regularSeasonGames = await GameRepository.FindGamesAsync(seasonId, GameType.RegularSeason, null);
            var playoffGames = new List<GameInfo>();
            var sideOne = new List<Team>();
            var sideTwo = new List<Team>();
            var superBowlTeams = new List<int>();
            foreach (var conference in conferences)
            {
                var date = gameDate;
                var playoffTeams = await GetPostSeasonTeamsAsync(seasonId, conference.Id);
                var winnerSeeds = new List<int>();

                // wild card round
                var winner = await GenerateVictoriousTeamAsync(
                    playoffTeams.GetValueOrDefault(3),
                    playoffTeams.GetValueOrDefault(6),
                    displayOutcome,
                    Constants.PlayoffSeasonHomeWinChance);
                var game = new GameInfo
                    {
                        StartTime = date,
                        GameType = GameType.PostSeason,
                        HomeTeamId = playoffTeams.GetValueOrDefault(3).Id,
                        VisitorTeamId = playoffTeams.GetValueOrDefault(6).Id,
                        VictoriousTeamId = winner
                    };
                var winnerSeed = game.VictoriousTeamId == game.HomeTeamId ? 3 : 6;
                winnerSeeds.Add(winnerSeed);
                playoffGames.Add(game);

                winner = await GenerateVictoriousTeamAsync(
                    playoffTeams.GetValueOrDefault(4),
                    playoffTeams.GetValueOrDefault(5),
                    displayOutcome,
                    Constants.PlayoffSeasonHomeWinChance);
                game = new GameInfo
                    {
                        StartTime = date,
                        GameType = GameType.PostSeason,
                        HomeTeamId = playoffTeams.GetValueOrDefault(4).Id,
                        VisitorTeamId = playoffTeams.GetValueOrDefault(5).Id,
                        VictoriousTeamId = winner
                    };
                winnerSeed = game.VictoriousTeamId == game.HomeTeamId ? 4 : 5;
                winnerSeeds.Add(winnerSeed);
                winnerSeeds = winnerSeeds.OrderByDescending(w => w).ToList();
                playoffGames.Add(game);

                date = date.AddDays(7);

                // Divisonal round

                var divisionWinnerSeeds = new List<int>();
                var divisionalWinner = await GenerateVictoriousTeamAsync(
                    playoffTeams.GetValueOrDefault(1),
                    playoffTeams.GetValueOrDefault(winnerSeeds.ElementAt(1)),
                    displayOutcome,
                    Constants.PlayoffSeasonHomeWinChance);
                var divisionGame = new GameInfo
                    {
                        StartTime = date,
                        GameType = GameType.PostSeason,
                        HomeTeamId = playoffTeams.GetValueOrDefault(1).Id,
                        VisitorTeamId = playoffTeams.GetValueOrDefault(winnerSeeds.ElementAt(1)).Id,
                        VictoriousTeamId = divisionalWinner
                    };
                var divisionWinnerSeed = divisionGame.VictoriousTeamId == divisionGame.HomeTeamId ? 1 : winnerSeeds.ElementAt(1);
                divisionWinnerSeeds.Add(divisionWinnerSeed);
                playoffGames.Add(divisionGame);

                divisionalWinner = await GenerateVictoriousTeamAsync(
                    playoffTeams.GetValueOrDefault(2),
                    playoffTeams.GetValueOrDefault(winnerSeeds.ElementAt(0)),
                    displayOutcome,
                    Constants.PlayoffSeasonHomeWinChance);
                divisionGame = new GameInfo
                    {
                        StartTime = date,
                        GameType = GameType.PostSeason,
                        HomeTeamId = playoffTeams.GetValueOrDefault(2).Id,
                        VisitorTeamId = playoffTeams.GetValueOrDefault(winnerSeeds.ElementAt(0)).Id,
                        VictoriousTeamId = divisionalWinner
                    };
                divisionWinnerSeed = divisionGame.VictoriousTeamId == divisionGame.HomeTeamId ? 2 : winnerSeeds.ElementAt(0);
                divisionWinnerSeeds.Add(divisionWinnerSeed);
                divisionWinnerSeeds = divisionWinnerSeeds.OrderByDescending(w => w).ToList();
                playoffGames.Add(divisionGame);

                // Conference Championship
                date = date.AddDays(7);

                var conferenceWinner = await GenerateVictoriousTeamAsync(
                    playoffTeams.GetValueOrDefault(divisionWinnerSeeds.ElementAt(0)),
                    playoffTeams.GetValueOrDefault(divisionWinnerSeeds.ElementAt(1)),
                    displayOutcome,
                    Constants.PlayoffSeasonHomeWinChance);
                var conferenceGame = new GameInfo
                    {
                        StartTime = date,
                        GameType = GameType.PostSeason,
                        HomeTeamId = playoffTeams.GetValueOrDefault(divisionWinnerSeeds.ElementAt(0)).Id,
                        VisitorTeamId = playoffTeams.GetValueOrDefault(divisionWinnerSeeds.ElementAt(1)).Id,
                        VictoriousTeamId = conferenceWinner
                    };
                superBowlTeams.Add(conferenceGame.VictoriousTeamId.Value);
                playoffGames.Add(conferenceGame);
            }

            var superBowlDate = gameDate.AddDays(21);

            // SuperBowl
            
            var homeTeam = teams.First(t => t.Id == superBowlTeams.ElementAt(0));
            var awayTeam = teams.First(t => t.Id == superBowlTeams.ElementAt(1));
            var winningTeam = Random.Next(0, 2) == 0 ? homeTeam : awayTeam;
            playoffGames.Add(
                new GameInfo
                {
                    StartTime = superBowlDate,
                    GameType = GameType.PostSeason,
                    HomeTeamId = homeTeam.Id,
                    VisitorTeamId = awayTeam.Id,
                    VictoriousTeamId = winningTeam.Id
                }
            );

            if (displayOutcome)
            {
                await FileLogger.WriteLineAsync($"SuperBowl bewteewn {homeTeam.City} {homeTeam.Name} and {awayTeam.City} {awayTeam.Name}; Winner: {winningTeam.City} {winningTeam.Name};");
            }
            
            await FileLogger.WriteLineAsync($"{winningTeam.City} {winningTeam.Name}", Constants.SuperBowlFile);

            foreach (var game in playoffGames)
            {
                await GameRepository.CreateGameAsync(game);
            }
        }

        public async Task<Dictionary<int, Team>> GetPostSeasonTeamsAsync(int seasonId, int conferenceId)
        {
            var postSeasonTeams = new Dictionary<int, Team>();
            var divisions = await DivisionRepository.GetDivisionsAsync();
            var teams = await TeamRepository.GetTeamsAsync();
            var conferenceDivisions = divisions.Where(d => d.ConferenceId == conferenceId).ToList();
            var regularSeasonGames = await GameRepository.FindGamesAsync(seasonId, GameType.RegularSeason, null);
            var conferenceTeams = new List<Team>();
            var playoffTeams = new List<Team>();
            foreach (var division in conferenceDivisions)
            {
                var divisionTeams = teams.Where(t => t.DivisionId == division.Id);
                conferenceTeams.AddRange(divisionTeams);
                var highestWinningTeam = await GetLeadingTeamAsync(seasonId, divisionTeams.ToList());
                playoffTeams.Add(highestWinningTeam);
            }
            var divisionLeaders = playoffTeams.ToList();
            conferenceTeams = conferenceTeams.Where(t => !playoffTeams.Select(pT =>pT.Id).Contains(t.Id)).ToList();
            var place = 0;
            do
            {
                var leadingTeam = await GetLeadingTeamAsync(seasonId, divisionLeaders);
                postSeasonTeams.Add(++place, leadingTeam);
                divisionLeaders.Remove(leadingTeam);
            }
            while (divisionLeaders.Count > 0);

            var wildCardTeam = await GetLeadingTeamAsync(seasonId, conferenceTeams);
            postSeasonTeams.Add(++place, wildCardTeam);
            conferenceTeams.Remove(wildCardTeam);
            wildCardTeam = await GetLeadingTeamAsync(seasonId, conferenceTeams);
            postSeasonTeams.Add(++place, wildCardTeam);

            return postSeasonTeams;
        }

        private async Task<int> GenerateVictoriousTeamAsync(Team homeTeam, Team visitorTeam, bool displayOutcome, int homeWinChance)
        {
            var victoriousTeam = Random.Next(0, 100) < homeWinChance ? homeTeam : visitorTeam;

            if (displayOutcome)
            {
                await FileLogger.WriteLineAsync($"Home: {homeTeam.City} {homeTeam.Name}; Visitor: {visitorTeam.City} {visitorTeam.Name}; Winner: {victoriousTeam.City} {victoriousTeam.Name};");
            }

            return victoriousTeam.Id;
        }

        private async Task<Team> GetLeadingTeamAsync(int seasonId, List<Team> teams)
        {
            var highestWinningTeam = default(Team);
            var highestWinningCount = -1;
            foreach (var team in teams)
            {
                var victories = await GameRepository.FindGamesAsync(
                    seasonId,
                    GameType.RegularSeason,
                    team.Id);
                
                if (victories.Count > highestWinningCount)
                {
                    highestWinningCount = victories.Count;
                    highestWinningTeam = team;
                }
                else if (victories.Count == highestWinningCount)
                {
                    // TieBreaker
                    if (Random.Next(0, 2) == 0)
                    {
                        highestWinningTeam = team;
                    }
                }
            }

            return highestWinningTeam;
        }

        private Random Random => new Random();
        private IConferenceRepository ConferenceRepository { get; }
        private IDivisionRepository DivisionRepository { get; }
        private ITeamRepository TeamRepository { get; }
        private IGameRepository GameRepository { get; }
        private IFileLogger FileLogger { get; }
    }
}