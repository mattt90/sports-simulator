namespace SportSimulator.Generators
{
    using System;
    using System.Threading.Tasks;
    using System.Collections.Generic;
    using SportSimulator.Models;

    public interface IPostSeasonGameGenerator
    {
        Task GeneratePostSeaonGamesAsync(int seasonId, DateTimeOffset gameDate, bool displayOutcome = false);
        Task<Dictionary<int, Team>> GetPostSeasonTeamsAsync(int seasonId, int conferenceId);
    }
}
