namespace SportSimulator.Loggers
{
    using System.IO;
    using System.Threading.Tasks;

    public class FileLogger : IFileLogger
    {
        public async Task WriteLineAsync(string value, string path = Constants.OutputFile)
        {
            using (var outputFile = new StreamWriter(path, true))
            {
                await outputFile.WriteLineAsync(value);
            }
        }
    }
}