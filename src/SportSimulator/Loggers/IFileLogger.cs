namespace SportSimulator.Loggers
{
    using System.Threading.Tasks;

    public interface IFileLogger
    {
        Task WriteLineAsync(string value, string path = Constants.OutputFile);
    }
}