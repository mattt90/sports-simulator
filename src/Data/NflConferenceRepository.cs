namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class NflConferenceRepository : IConferenceRepository
    {
        public async Task<List<Conference>> GetConferencesAsync()
        {
            if (Conferences?.Any() != true)
            {
                Conferences = DefaultConferences;
            }

            return await Task.FromResult(Conferences);
        }
        private List<Conference> Conferences { get; set; }
        private static List<Conference> DefaultConferences = new List<Conference>
            {
                new Conference
                {
                    Id = 1,
                    Name = "AFC",
                    LeagueId = 1
                },
                new Conference
                {
                    Id = 2,
                    Name = "NFC",
                    LeagueId = 1
                }
            };
    }
}
