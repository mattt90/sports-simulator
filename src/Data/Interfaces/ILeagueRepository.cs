namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ILeagueRepository
    {
        Task<List<League>> GetLeaguesAsync();
    }
}