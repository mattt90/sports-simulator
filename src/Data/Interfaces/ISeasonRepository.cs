namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ISeasonRepository
    {
        Task<List<Season>> GetSeasonsAsync();

        Task<int> CreateSeasonAsync(SeasonInfo seasonInfo);
    }
}