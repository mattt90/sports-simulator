namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IGameRepository
    {
        Task<List<Game>> FindGamesAsync(int? seasonId, GameType? gameType, int? victoriousTeamId);
        Task<int> CreateGameAsync(GameInfo gameInfo);
    }
}
