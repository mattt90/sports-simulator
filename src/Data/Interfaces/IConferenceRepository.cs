namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IConferenceRepository
    {
        Task<List<Conference>> GetConferencesAsync();
    }
}