namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ITeamRepository
    {
        Task<List<Team>> GetTeamsAsync();
    }
}
