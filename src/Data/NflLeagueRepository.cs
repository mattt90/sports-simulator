namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class NflLeagueRepository : ILeagueRepository
    {
        public async Task<List<League>> GetLeaguesAsync()
        {
            if (Leagues?.Any() != true)
            {
                Leagues = DefaultLeagues;
            }

            return await Task.FromResult(Leagues);
        }
        private List<League> Leagues { get; set; }
        private static List<League> DefaultLeagues = new List<League>
            {
                new League
                {
                    Id = 1,
                    Name = "NFL",
                    Sport = Sport.Football,
                    TeamMaxSize = 60,
                }
            };
    }
}
