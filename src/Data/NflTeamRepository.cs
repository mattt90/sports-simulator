namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class NflTeamRepository : ITeamRepository
    {
        public async Task<List<Team>> GetTeamsAsync()
        {
            if (Teams?.Any() != true)
            {
                Teams = DefaultTeams;
            }

            return await Task.FromResult(Teams);
        }
        private List<Team> Teams { get; set; }
        private static List<Team> DefaultTeams = new List<Team> 
            {
                new Team
                {
                    Id = 1,
                    DivisionId = 6,
                    Name = "Packers",
                    Abbreviation = "GB",
                    City = "Green Bay",
                    StadiumName = "Lambeau Field",
                    DivisionRank = 3
                },
                new Team
                {
                    Id = 2,
                    DivisionId = 1,
                    Name = "Bills",
                    City = "Buffalo",
                    DivisionRank = 3
                },
                new Team
                {
                    Id = 3,
                    DivisionId = 1,
                    Name = "Dolphins",
                    City = "Miami",
                    DivisionRank = 2
                },
                new Team
                {
                    Id = 4,
                    DivisionId = 1,
                    Name = "Patriots",
                    City = "New England",
                    DivisionRank = 1
                },
                new Team
                {
                    Id = 5,
                    DivisionId = 1,
                    Name = "Jets",
                    City = "New York",
                    DivisionRank = 4
                },
                new Team
                {
                    Id = 6,
                    DivisionId = 2,
                    Name = "Ravens",
                    City = "Baltimore",
                    DivisionRank = 1
                },
                new Team
                {
                    Id = 7,
                    DivisionId = 2,
                    Name = "Bengals",
                    City = "Cincinnati",
                    DivisionRank = 4
                },
                new Team
                {
                    Id = 8,
                    DivisionId = 2,
                    Name = "Browns",
                    City = "Cleveland",
                    DivisionRank = 3
                },
                new Team
                {
                    Id = 9,
                    DivisionId = 2,
                    Name = "Steelers",
                    City = "Pittsburgh",
                    DivisionRank = 2
                },
                new Team
                {
                    Id = 10,
                    DivisionId = 3,
                    Name = "Texans",
                    City = "Houston",
                    DivisionRank = 1
                },
                new Team
                {
                    Id = 11,
                    DivisionId = 3,
                    Name = "Colts",
                    City = "Indianapolis",
                    DivisionRank = 2
                },
                new Team
                {
                    Id = 12,
                    DivisionId = 3,
                    Name = "Jaguars",
                    City = "Jacksonville",
                    DivisionRank = 4
                },
                new Team
                {
                    Id = 13,
                    DivisionId = 3,
                    Name = "Titans",
                    City = "Tennessee",
                    DivisionRank = 3
                },
                new Team
                {
                    Id = 14,
                    DivisionId = 4,
                    Name = "Broncos",
                    City = "Denver",
                    DivisionRank = 3
                },
                new Team
                {
                    Id = 15,
                    DivisionId = 4,
                    Name = "Chiefs",
                    City = "Kansas City",
                    DivisionRank = 1
                },
                new Team
                {
                    Id = 16,
                    DivisionId = 4,
                    Name = "Chargers",
                    City = "Los Angeles",
                    DivisionRank = 2
                },
                new Team
                {
                    Id = 17,
                    DivisionId = 4,
                    Name = "Raiders",
                    City = "Oakland",
                    DivisionRank = 4
                },
                new Team
                {
                    Id = 18,
                    DivisionId = 5,
                    Name = "Cowboys",
                    City = "Dallas",
                    DivisionRank = 1
                },
                new Team
                {
                    Id = 19,
                    DivisionId = 5,
                    Name = "Giants",
                    City = "New York",
                    DivisionRank = 4
                },
                new Team
                {
                    Id = 20,
                    DivisionId = 5,
                    Name = "Eagles",
                    City = "Philadelphia",
                    DivisionRank = 2
                },
                new Team
                {
                    Id = 21,
                    DivisionId = 5,
                    Name = "Redskins",
                    City = "Washington",
                    DivisionRank = 3
                },
                new Team
                {
                    Id = 22,
                    DivisionId = 6,
                    Name = "Bears",
                    City = "Chicago",
                    DivisionRank = 1
                },
                new Team
                {
                    Id = 23,
                    DivisionId = 6,
                    Name = "Lions",
                    City = "Detroit",
                    DivisionRank = 4
                },
                new Team
                {
                    Id = 24,
                    DivisionId = 6,
                    Name = "Vikings",
                    City = "Minnesota",
                    DivisionRank = 2
                },
                new Team
                {
                    Id = 25,
                    DivisionId = 7,
                    Name = "Falcons",
                    City = "Atlanta",
                    DivisionRank = 2
                },
                new Team
                {
                    Id = 26,
                    DivisionId = 7,
                    Name = "Panthers",
                    City = "Carolina",
                    DivisionRank = 3
                },
                new Team
                {
                    Id = 27,
                    DivisionId = 7,
                    Name = "Saints",
                    City = "New Orleans",
                    DivisionRank = 1
                },
                new Team
                {
                    Id = 28,
                    DivisionId = 7,
                    Name = "Buccaneers",
                    City = "Tampa Bay",
                    DivisionRank = 4
                },
                new Team
                {
                    Id = 29,
                    DivisionId = 8,
                    Name = "Cardinals",
                    City = "Arizona",
                    DivisionRank = 4
                },
                new Team
                {
                    Id = 30,
                    DivisionId = 8,
                    Name = "Rams",
                    City = "Los Angeles",
                    DivisionRank = 1
                },
                new Team
                {
                    Id = 31,
                    DivisionId = 8,
                    Name = "49ers",
                    City = "San Francisco",
                    DivisionRank = 3
                },
                new Team
                {
                    Id = 32,
                    DivisionId = 8,
                    Name = "Seahawks",
                    City = "Seatle",
                    DivisionRank = 2
                }
            };
    }
}
