namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class NflDivisionRepository : IDivisionRepository
    {
        public async Task<List<Division>> GetDivisionsAsync()
        {
            if (Divisions?.Any() != true)
            {
                Divisions = DefaultDivisions;
            }

            return await Task.FromResult(Divisions);
        }
        private List<Division> Divisions { get; set; }
        private static List<Division> DefaultDivisions = new List<Division>
            {
                new Division
                {
                    Id = 1,
                    Name = "AFC East",
                    ConferenceId = 1
                },
                new Division
                {
                    Id = 2,
                    Name = "AFC North",
                    ConferenceId = 1
                },
                new Division
                {
                    Id = 3,
                    Name = "AFC South",
                    ConferenceId = 1
                },
                new Division
                {
                    Id = 4,
                    Name = "AFC West",
                    ConferenceId = 1
                },
                new Division
                {
                    Id = 5,
                    Name = "NFC East",
                    ConferenceId = 2
                },
                new Division
                {
                    Id = 6,
                    Name = "NFC North",
                    ConferenceId = 2
                },
                new Division
                {
                    Id = 7,
                    Name = "NFC South",
                    ConferenceId = 2
                },
                new Division
                {
                    Id = 8,
                    Name = "NFC West",
                    ConferenceId = 2
                }
            };
    }
}
