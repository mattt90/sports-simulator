namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class NflSeasonRepository : ISeasonRepository
    {
        static NflSeasonRepository()
        {
            Index = 0;
        }

        public NflSeasonRepository()
        {
            Seasons = Seasons ?? new List<Season>();
        }

        public async Task<List<Season>> GetSeasonsAsync()
        {
            return await Task.FromResult(Seasons);
        }

        public async Task<int> CreateSeasonAsync(SeasonInfo seasonInfo)
        {
            var season = new Season
            {
                Id = Index++,
                Name = seasonInfo.Name,
                StartDate = seasonInfo.StartDate,
                LeagueId = seasonInfo.LeagueId
            };
            Seasons.Add(season);

            return await Task.FromResult(season.Id);
        }

        private List<Season> Seasons { get; set; }
        private static int Index { get; set; }
        private static List<Season> DefaultSeasons = new List<Season>
            {
                new Season
                {
                    Id = 1,
                    Name = "2019 Season",
                    LeagueId = 1,
                    StartDate = new DateTime(2019, 9, 5)
                }
            };
    }
}
