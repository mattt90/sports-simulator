namespace SportSimulator.Data
{
    using SportSimulator.Models;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class NflGameRepository : IGameRepository
    {
        static NflGameRepository()
        {
            Index = 0;
        }

        public NflGameRepository()
        {
            Games = Games ?? new List<Game>();
        }

        private static int Index { get; set; }
        public async Task<List<Game>> FindGamesAsync(
            int? seasonId,
            GameType? gameType,
            int? victoriousTeamId)
        {
            return await Task.FromResult(Games.Where(g => (seasonId == null || g.SeasonId == seasonId) && (gameType == null || g.GameType == gameType) && (victoriousTeamId == null || g.VictoriousTeamId == victoriousTeamId)).ToList());
        }

        public async Task<int> CreateGameAsync(GameInfo gameInfo)
        {
            var game = new Game
            {
                Id = Index++,
                HomeTeamId = gameInfo.HomeTeamId,
                VisitorTeamId = gameInfo.VisitorTeamId,
                VictoriousTeamId = gameInfo.VictoriousTeamId,
                StartTime = gameInfo.StartTime,
                SeasonId = gameInfo.SeasonId,
                GameType = gameInfo.GameType
            };
            Games.Add(game);

            return await Task.FromResult(game.Id);
        }

        private List<Game> Games { get; set; }
    }
}
