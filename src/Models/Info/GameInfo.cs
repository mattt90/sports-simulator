namespace SportSimulator.Models
{
    using System;

    public class GameInfo
    {
        public int HomeTeamId { get; set; }
        public int VisitorTeamId { get; set; }
        public DateTimeOffset StartTime { get; set; }
        public int? VictoriousTeamId { get; set; }
        public int SeasonId { get; set; }
        public GameType GameType { get; set; }
    }
}
