﻿namespace SportSimulator.Models
{
    using System;

    public class SeasonInfo
    {
        public string Name { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public int LeagueId { get; set; }
    }
}
