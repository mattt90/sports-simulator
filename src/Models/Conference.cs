﻿namespace SportSimulator.Models
{
    public class Conference
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int LeagueId { get; set; }
    }
}
