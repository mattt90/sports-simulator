﻿namespace SportSimulator.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Abbreviation { get; set; }
        public string City { get; set; }
        public string StadiumName { get; set; }
        public int SalaryCap { get; set; }
        public int DivisionId { get; set; }
        public int DivisionRank { get; set; }
    }
}
