﻿namespace SportSimulator.Models
{
    public enum GameType
    {
        PreSeason,
        RegularSeason,
        PostSeason
    }
}
