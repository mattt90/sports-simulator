﻿namespace SportSimulator.Models
{
    public enum Sport
    {
        Basketball,
        Baseball,
        Football
    }
}
