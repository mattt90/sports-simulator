﻿namespace SportSimulator.Models
{
    using System;

    public class Player
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int JerseyNumber { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public string Hometown { get; set; }
        public bool IsActive { get; set; }
        public int Position { get; set; }
        public DateTime BirthDate { get; set; }
        public int Experience { get; set; }
        public bool IsStarter { get; set; }
        public int Salary { get; set; }
    }
}
