﻿namespace SportSimulator.Models
{
    public class League
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TeamMaxSize { get; set; }
        public Sport Sport { get; set; }
    }
}
