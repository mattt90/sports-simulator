﻿namespace SportSimulator.Models
{
    public class Division
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ConferenceId { get; set; }
    }
}
